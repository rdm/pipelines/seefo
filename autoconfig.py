#! /usr/bin/env python
# -*- coding: utf-8 -*-
import warnings
import yaml
import click
import pandas as pd

YAML_FILE = "autoconfig.yml"


def get_config(config, var):
    for value in config["variables"].values():
        if var in value["aliases"]:
            return value


def get_variable(config, var):
    for key, value in config["variables"].items():
        if var in value["aliases"]:
            return key


@click.command()
@click.option("-i", "--l1file", type=click.Path(exists=True), required=True)
@click.option("-m", "--maintenancefile", type=click.Path(), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
def main(infile, maintenancefile, outfile):

    with open(YAML_FILE, "r", encoding="utf-8") as f:
        spec = yaml.safe_load(f)

    variables: pd.Index = pd.read_parquet(infile).index.get_level_values(0).drop_duplicates()

    saqc_config = ["# SaQC\n\n## Variables\n"]
    saqc_groups = ["## flag transfers"]

    var_map = {}
    for var in variables:

        key = get_variable(spec, var)
        if key is None:
            if var not in spec["ignore"]["variables"]:
                # warnings.warn(f"variable {var} not found in configuration")
                print(f"WARN: '{var}' not found in configuration")
            continue

        var_map[key] = var
        config = spec["variables"][key]
        calls = (
            "\n".join(
                [f"### {key}"] + [f"{var} ; {c}" for c in config["calls"]]
            ).format(maintenancefile)
            + "\n"
        )
        saqc_config.append(calls)

        if group := config["group"]:
            group = [var_map[v] for v in sum(group, []) if v in var_map]
            if group:
                saqc_groups.append(f"{var} ; orGroup(group={group})")

    with open(outfile, "w") as f:
        config_section = "\n".join(saqc_config)
        group_section = "\n".join(saqc_groups)
        f.write("\n".join([config_section, group_section]) + "\n")


if __name__ == "__main__":
    main()
