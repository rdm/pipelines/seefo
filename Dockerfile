FROM python:3.9

RUN git clone --recursive https://git.ufz.de/rdm/pipelines/seefo /pipeline

WORKDIR /pipeline

RUN mkdir .cache

ENV PYTHONPATH=saqc/:pipetools/:$PYTHONPATH

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r saqc/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

CMD snakemake -j1 --forceall --printshellcmds --keep-going --restart-times 3
