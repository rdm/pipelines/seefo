#! /usr/bin/env python
# -*- coding: utf-8 -*-

from functools import partial

import pandas as pd
import click

from pipetools.lib import writeParquet


def filterInvalid(maint):
    maint = maint.sort_index()
    drops = []
    for k in range(0, (maint.shape[0] - 1)):
        if maint.values[k] >= maint.index[k + 1]:
            drops.append(maint.index[k])

    return maint.drop(drops)


def retrieveMaint(
    infile: str,
    marker: str,
    label: str,
    station_col: int,
) -> pd.DataFrame:
    """
    :param infile:        Path to protocol .xlsx file
    :param label:         Abbreviation of the maintained logger/station
    :param marker:        Marker in eventcolumn, indicating that event has occured

    :return: maint:       representing maintenance interval.
                          (pd.DataFrame(values=maint_end_time, index=maint_start_time)
    """
    if not label:
        return pd.Series([], index=pd.DatetimeIndex([]), dtype=float)

    stations = pd.read_excel(infile, sheet_name=0, usecols=[station_col], squeeze=True)

    dates = pd.read_excel(infile, sheet_name=0, usecols=[3], dtype=str, squeeze=True)
    start = pd.read_excel(infile, sheet_name=0, usecols=[4], dtype=str, squeeze=True)
    end = pd.read_excel(infile, sheet_name=0, usecols=[5], dtype=str, squeeze=True)
    clean = pd.read_excel(infile, sheet_name=0, usecols=[7], dtype=str, squeeze=True)

    dates = pd.to_datetime(dates, errors="coerce").dt.strftime("%Y-%m-%d")
    d_start = pd.to_datetime(dates + "T" + start, errors="coerce")
    d_end = pd.to_datetime(dates + "T" + end, errors="coerce")

    maint = pd.Series(d_end.values, index=d_start, name=f"maintenance")
    maint = maint.loc[((stations == label) & (clean == marker)).values]
    maint = maint[maint.index.notnull()].dropna().to_frame()

    return maint


FUNC_MAP = {
    36: partial(retrieveMaint, marker="x", station_col=1),
    63: partial(retrieveMaint, marker="J", station_col=0),
}


@click.command()
@click.option("-i", "--infile", type=click.Path(exists=True), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
@click.option("-p", "--project", type=int, required=True, help="project id")
@click.option("-l", "--label", type=str, required=True, help="logger abbreviation label")
def main(project, infile, outfile, label):
    data = FUNC_MAP[project](infile, label=label)
    out = filterInvalid(data)
    writeParquet(data, outfile)


if __name__ == "__main__":
    main()
