#! /usr/bin/env python
# -*- coding: utf-8 -*-

from uuid import uuid4

import click
import numpy as np
import pandas as pd

from saqc import fromConfig
from saqc.core import flagging
from saqc.funcs import processGeneric, flagByStatLowPass, transferFlags, dropField
from saqc.lib.ts_operators import butterFilter

from pipetools.lib import splitData, mergeData, writeParquet, diosUnstack, resultStack


def getManFlags(fname, field):

    mflags = pd.read_csv(fname, sep=";", comment="#")
    mflags = mflags.apply(lambda s: s.str.strip())
    mflags.columns = mflags.columns.str.strip()
    mflags = mflags.set_index("variable", drop=True)

    out = mflags[mflags.index == field]
    out["start_date"] = pd.to_datetime(out["start_date"])
    out["end_date"] = pd.to_datetime(out["end_date"])
    return out


@flagging()
def flagFromFile(data, field, flags, fname, **kwargs):
    mflags = getManFlags(fname, field)
    for _, (start, end, flag) in mflags.iterrows():
        mask = pd.Series(data=0, index=data[field].index, dtype=bool)
        mask.loc[start:end] = True
        flags[mask, field] = flag
    return data, flags


@flagging()
def flagNoise(data, field, flags, cutoff, window, sub_window, thresh, sub_thresh, **kwargs):
    tmp_field = str(uuid4())
    data, flags = processGeneric(data, field, flags, target=tmp_field, func=lambda x: butterFilter(x, cutoff=cutoff) - x)
    data, flags = flagByStatLowPass(data, tmp_field, flags, np.std, window=window, sub_window=sub_window, thresh=thresh, subt_thresh=sub_thresh)
    data, flags = transferFlags(data, tmp_field, flags, target=field)
    data, flags = dropField(data, tmp_field, flags)
    return data, flags


@click.command()
@click.option("-i", "--infile", type=click.Path(exists=True), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
@click.option("-c", "--configfile", type=click.Path(exists=True), required=True)
def main(infile, outfile, configfile):

    data = pd.read_parquet(infile)
    data = diosUnstack(data)

    saqc = fromConfig(configfile, data=data, scheme="dmp")
    df_out = resultStack(saqc.result)

    writeParquet(df_out, outfile)


if __name__ == "__main__":
    main()
