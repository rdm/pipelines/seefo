# SEEFO

Repository, comprosing quality assurance pipeline for data collected at the Dam Observatory Rappbode (TOR), collected by the Institute for Lake Research Magdeburg (SEEFO).
 
Get a project overview here: https://confluence.digitalearth-hgf.de/display/RDM/SEEFO+Pipeline

## Usage

Get and run pipeline
```
docker run --env-file /path/to/file.env --network host --rm git.ufz.de:4567/rdm/pipelines/seefo:0.1 snakemake -j2 --forceall --keep-going --restart-times 3
```
