#! /usr/bin/env python
# -*- coding: utf-8 -*-

import click
import pandas as pd

from saqc import SaQC, UNFLAGGED, fromConfig
from saqc.core import register, DmpScheme, Flags
from saqc.core.reader import readFile as readConfigFile
from saqc.funcs.drift import correctDrift
from saqc.funcs.tools import dropField

from pipetools.lib import splitData, mergeData, writeParquet, diosUnstack, resultStack


def registerWrapper(maintdata: pd.Series) -> None:

    @register(mask=["field"], demask=[], squeeze=[])
    def correctExponentialDrift(data, field, flags, **kwargs):
        maint_field = "maintenance"
        data[maint_field] = maintdata
        flags[maint_field] = pd.Series(UNFLAGGED, index=maintdata.index)
        data, flags = correctDrift(
            data,
            field,
            flags,
            maintenance_field=maint_field,
            model="exponential",
            **kwargs
        )
        return dropField(data, maint_field, flags)


def translateStackedFlags(flags):
    flags_ = {}
    translator = DmpScheme()
    for k in flags.index.get_level_values(0).drop_duplicates():
        flags_[k] = translator.toHistory(flags.loc[k])
    return Flags(flags_)


@click.command()
@click.option("-i", "--infile", type=click.Path(exists=True), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
@click.option("-c", "--configfile", type=click.Path(exists=True), required=True)
@click.option("-m", "--maintfile", type=click.Path(exists=True), required=True)
def main(infile, outfile, configfile, maintfile):

    maintdata = pd.read_parquet(maintfile)
    registerWrapper(maintdata.squeeze())

    df = pd.read_parquet(infile)
    data, flags = splitData(df)

    data_ = diosUnstack(data)
    flags_ = translateStackedFlags(flags)

    saqc = fromConfig(configfile, data=data_, flags=flags_, scheme="dmp")

    df_out = resultStack(saqc.result)
    # get only the corrected fields
    fields = readConfigFile(configfile)["varname"].tolist()
    writeParquet(df_out.loc[fields], outfile)


if __name__ == "__main__":
    main()
