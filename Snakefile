configfile: "./config.yaml"

PATH = ".cache"
LOGGER_IDS = config["loggers"]
MAINTENANCE_FILES = config["maintenance"]


wildcard_constraints:
    logger_id="\d+",
    project_id="\d+"


def getTargets():
    targets = []
    for logger_id, logger_meta in LOGGER_IDS.items():
        project_id = logger_meta["project_id"]
        targets.append(f"{PATH}/{project_id}_{logger_id}_quality.upload")
        if logger_meta["drift_logger_id"]:
            targets.append(f"{PATH}/{project_id}_{logger_id}_drift.upload")

    return targets


rule all:
    input: getTargets()

rule extractNextcloud:
    params:
        remote=lambda wcs, *args: MAINTENANCE_FILES[int(wcs[1])],
    output: temp("{PATH}/protocoll_{project_id}.xlsx")
    shell: "python pipetools/extractNextcloud.py --infile '{params.remote}' --outfile {output}"

rule extractMaintenance:
    params:
        label = lambda wcs, *args: LOGGER_IDS[int(wcs[2])]["label"]
    input: "{PATH}/protocoll_{project_id}.xlsx"
    output: temp("{PATH}/maintenance_{project_id}_{logger_id}.parquet")
    shell: "python extractMaintenance.py --infile {input} --outfile {output} --project {wildcards.project_id} --label {params.label}"

rule extractLevel1:
    params:
        project_id="{project_id}",
        logger_id="{logger_id}"
    threads: workflow.cores
    output: "{PATH}/{project_id}_{logger_id}_level1.parquet"
    shell:  "python pipetools/extractLogger.py --project {params.project_id} --logger {params.logger_id} --level 1 --outfile {output}"

rule extractLevel2:
    params:
        project_id="{project_id}",
        logger_id="{logger_id}"
    threads: workflow.cores
    output: "{PATH}/{project_id}_{logger_id}_level2.parquet"
    shell:  "python pipetools/extractLogger.py --project {params.project_id} --logger {params.logger_id} --level 2 --outfile {output}"

rule merge:
    input:
        level1="{PATH}/{project_id}_{logger_id}_level1.parquet",
        level2="{PATH}/{project_id}_{logger_id}_level2.parquet",
    output: temp("{PATH}/{project_id}_{logger_id}_base.parquet")
    shell:  "python pipetools/mergeLevels.py --l1file {input.level1} --l2file {input.level2} --outfile {output}"

rule qualityControl:
    input: "{PATH}/{project_id}_{logger_id}_level1.parquet",
    output: temp("{PATH}/{project_id}_{logger_id}_quality.parquet")
    shell:  "python qcontrolLogger.py --infile {input} --outfile {output} --configfile configs/qc/{wildcards.logger_id}.csv"

rule driftCorrection:
    input:
        data = "{PATH}/{project_id}_{logger_id}_quality.parquet",
        maint = "{PATH}/maintenance_{project_id}_{logger_id}.parquet"
    output: temp("{PATH}/{project_id}_{logger_id}_drift.parquet")
    shell:  "python correctLogger.py --infile {input.data} --outfile {output} --configfile configs/drift/{wildcards.logger_id}.csv --maintfile {input.maint}"

rule loadQualityControl:
    params:
        project_id="{project_id}",
        logger_id="{logger_id}"
    input:
        saqc="{PATH}/{project_id}_{logger_id}_quality.parquet", # find the correct saqc file
        base="{PATH}/{project_id}_{logger_id}_base.parquet",
    priority: 1
    output: touch("{PATH}/{project_id}_{logger_id}_quality.upload")
    shell: "python pipetools/loadLogger.py --infile {input.saqc} --basefile {input.base} --project {params.project_id} --logger {params.logger_id} --level 2 --force-run"

rule loadDriftCorrection:
    params:
        project_id=lambda wc, *args: LOGGER_IDS[int(wc[2])]["drift_project_id"],
        logger_id=lambda wc, *args: LOGGER_IDS[int(wc[2])]["drift_logger_id"]
    input:
        saqc="{PATH}/{project_id}_{logger_id}_drift.parquet",
        base=lambda wcs: f"{PATH}/{LOGGER_IDS[int(wcs[2])]['drift_project_id']}_{LOGGER_IDS[int(wcs[2])]['drift_logger_id']}_base.parquet",
    output: touch("{PATH}/{project_id}_{logger_id}_drift.upload")
    shell: "python pipetools/loadLogger.py --infile {input.saqc} --basefile {input.base} --project {params.project_id} --logger {params.logger_id} --level both --force-run"

onsuccess:
    shell("python pipetools/sendMail.py --sender david.schaefer@ufz.de --recipient david.schaefer@ufz.de --subject 'SEEFO/ASAM Pipeline: FINISHED'")

onerror:
    shell("cat {log} | python pipetools/sendMail.py --sender david.schaefer@ufz.de --recipient david.schaefer@ufz.de --subject 'SEEFO/ASAM Pipeline: FAILED' --stdin")
